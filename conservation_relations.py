from sympy import Matrix
import numpy as np
__author__ = 'Tim Rose'
__status__ = 'development'
__email__ = 'tim.rose@hhu.de'
__credits__ = ['Oliver Ebenhoeh']

# Stoichiometry matrix of the pentose phosphate Pathway
# Including G6P, S12P, O8P and O18P

# Reactions:
#             R   R   T   T   T   A   T   T   A   P   P
#             P   P   K   A   K   D   P   K   D   T   G
#             I   E   1   1   2   1   I   3   2       I
S = Matrix([[ 0,  0,  1, -1,  1,  0, -1,  0,  0,  0,  0],  # GAP
            [ 0,  0,  0,  0,  0, -1,  1,  0, -1,  0,  0],  # DHAP
            [ 0,  0,  0,  1, -1, -1,  0,  1,  0,  0,  0],  # E4P
            [ 0,  1, -1,  0, -1,  0,  0,  0,  0,  0,  0],  # X5P
            [-1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0],  # Ru5P
            [ 1,  0, -1,  0,  0,  0,  0,  0, -1,  0,  0],  # R5P
            [ 0,  0,  0,  0,  0,  0,  0, -1,  0,  0, -1],  # G6P
            [ 0,  0,  0,  1,  1,  0,  0, -1,  0,  0,  1],  # F6P
            [ 0,  0,  1, -1,  0,  0,  0,  0,  0, -1,  0],  # S7P
            [ 0,  0,  0,  0,  0,  1,  0,  0,  0,  1,  0],  # S17P
            [ 0,  0,  0,  0,  0,  0,  0,  1,  0,  1,  0],  # O8P
            [ 0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0]])  # O18P

# Calculation of the two conservation Relations:
ns = S.transpose().nullspace()
f = np.array(ns[0]).transpose()
s = np.array(ns[1]).transpose()
print('A:')
print(f)
print('B:')
print(s)
print(' ')
print('C1:')
print(8*s+8*f)
print('C2:')
print((4*f + 8*s)/4)
