#!/usr/bin/python3

from math import exp, sqrt
from scipy import optimize
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
from matplotlib.backends.backend_pdf import PdfPages

__author__ = 'Tim Rose'
__status__ = 'development'
__email__ = ['tim.rose@hhu.de', 'oliver.ebenhoeh@hhu.de']
__credits__ = ['Oliver Ebenhoeh', 'Anna Matuszynska']


class MissingDataError(Exception):
    pass


class ConstraintError(Exception):
    pass


class PPPmodel:
    """ Equilibrium-Module of the non-oxidative Part of the Pentose Phosphate Pathway.
    Given the amount of phosphates and carbon in the system, it is able to calculate the equilibrium distribution
    of included sugar-phosphate (GAP, DHAP, E4P, X5P, Ru5P, R5P, G6P, F6P, S7P, S17P, O8P, O18P)"""

    def __init__(self, par=(-287.36023322, -287.39097921)): # energy of formation for O8P and O18P are numerically estimated
        self.dGf = {'gap': [-102.46, 0.], 'dhap': [-104.28, 0.], 'e4p': [-139.14, 1.], 'r5p': [-177.58, 2.],
                    'ru5p': [-177.04, 2.], 'x5p': [-177.28, 2.], 'g6p': [-215.93, 3.], 'f6p': [-215.43, 3.],
                    's7p': [-252.49, 4.], 's17p': [-249.05, 4.], 'o8p': [par[0], 5.],
                    'o18p': [par[1], 5.]}  # kcal/mol
        self.T = 273.15+25  # K
        self.R = 8.314459  # J/mol*K
        self.KCALinJ = 4.1868*1000  # Conversion factor from kcal to Joule

        self.nu = {}
        self. kZero = 'gap'
        self.kOne = 'e4p'
        for i in self.dGf:
            if i != self.kZero and i != 's17p' and i != 'o18p':
                self.nu[i] = self.dGf[i][0] - self.dGf[self.kZero][0] - self.dGf[i][1]*(self.dGf[self.kOne][0] -
                                                                                        self.dGf[self.kZero][0])
        self.nu['s17p'] = self.dGf['s17p'][0] - 2 * self.dGf[self.kZero][0] - (self.dGf[self.kOne][0] -
                                                                               self.dGf[self.kZero][0])
        self.nu['o18p'] = self.dGf['o18p'][0] - 2 * self.dGf[self.kZero][0] - 2*(self.dGf[self.kOne][0] -
                                                                                 self.dGf[self.kZero][0])

        # final Parameters for the EQ Module
        self.k3 = exp(-self.nu['dhap'] * self.KCALinJ / (self.R * self.T))
        self.k43 = exp(-self.nu['e4p'] * self.KCALinJ / (self.R * self.T))
        self.k5 = exp((self.dGf['x5p'][0] - self.dGf['ru5p'][0]) * self.KCALinJ / (self.R * self.T))
        self.k5s = exp((self.dGf['x5p'][0] - self.dGf['r5p'][0]) * self.KCALinJ / (self.R * self.T))
        self.k53 = exp(-self.nu['x5p'] * self.KCALinJ / (self.R * self.T))
        self.k63 = exp(-self.nu['g6p'] * self.KCALinJ / (self.R * self.T))
        self.k6 = exp((self.dGf['g6p'][0] - self.dGf['f6p'][0]) * self.KCALinJ / (self.R * self.T))
        self.k73 = exp(-self.nu['s7p'] * self.KCALinJ / (self.R * self.T))
        self.k17 = exp(-self.nu['s17p'] * self.KCALinJ / (self.R * self.T))
        self.k83 = exp(-self.nu['o8p'] * self.KCALinJ / (self.R * self.T))
        self.k18 = exp(-self.nu['o18p'] * self.KCALinJ / (self.R * self.T))
        self.f3 = 1 + self.k3
        self.f5 = 1 + self.k5 + self.k5s
        self.f6 = 1 + self.k6


    @staticmethod
    def constraints(C, P):
        if 3*P < C < 8*P:
            return C, P
        elif C == 0 and P == 0:
            return C, P
        else:
            raise ConstraintError("\n\nConstraint ERROR:\nConstraint " + '\033[1m' + "3*P < C < 8*P" + '\033[0m' +
                                  " , was violated\n")

    def solve_x3(self, z, P):
        p = (self.f3 + self.k43*z + self.f5*self.k53*(z**2) + self.f6*self.k63*(z**3) + self.k73*(z**4) +
             self.k83*(z**5)) / (2*(self.k17*z + self.k18*(z**2)))

        q = -P / (2*(self.k17*z + self.k18*(z**2)))

        return (-p / 2) + sqrt(((p**2) / 4) - q)

    def solve_z(self, z, C, P):
        return (self.solve_x3(z, P)**2)*(7*self.k17*z + 8*self.k18*(z**2)) + \
               self.solve_x3(z, P)*(3*self.f3 + 4*self.k43*z + 5*self.f5*self.k53*(z**2) +
                                    6*self.f6*self.k63*(z**3) + 7*self.k73*(z**4) + 8*self.k83*(z**5)) - C

    def solve(self, C, P, interval=(1e-100, 4)):
        C, P = self.constraints(C, P)

        try:
            z = optimize.brentq(self.solve_z, interval[0], interval[1], args=(C, P))
            return z
        except ValueError:
            raise ValueError('calculation near constraints failed!')

    def plot_z(self, C, P, space=(0, 10, 200)):
        """Plots the Z function over a given range"""
        if type(C) is list and type(P) is list:
            if len(C) == len(P):
                x = np.linspace(space[0], space[1], space[2])
                for i in range(len(C)):
                    y = []

                    for j in x:
                        y.append(self.solve_z(j, C[i], P[i]))
                    plt.plot(x, y, '.-', label=str(i))
                plt.legend()
                plt.xlabel('z')
                plt.ylabel('f(z)')
                plt.show()
            else:
                raise IndexError('\n\nC and P must have the same length\n\n')

        else:
            try:
                float(C)
                float(P)
                x = np.linspace(space[0], space[1], space[2])

                y = []
                for j in x:
                    y.append(self.solve_z(j, C, P))
                plt.plot(x, y, '.-')
                plt.xlabel('z')
                plt.ylabel('f(z)')
                plt.show()
            except TypeError:
                raise TypeError('\n\nERROR: C and P must be either lists or numbers\n\n')

    def get_all(self, C, P):
        if C == 0. and P == 0.:
            return {'gap': 0., 'dhap': 0., 'e4p': 0., 'x5p': 0., 'ru5p': 0., 'r5p': 0., 'g6p': 0., 'f6p': 0.,
                's7p': 0., 's17p': 0., 'o8p': 0., 'o18p': 0.}
        self.C = C
        self.P = P
        z = self.solve(C, P)
        # C3
        gap = self.solve_x3(z, P)
        dhap = self.k3 * gap
        # C4
        e4p = self.k43 * gap * z
        # C5
        x5p = gap * self.k53 * (z**2)
        ru5p = x5p * self.k5
        r5p = x5p * self.k5s
        # C6
        g6p = gap * self.k63 * (z ** 3)
        f6p = g6p * self.k6
        # C7
        s7p = gap * self.k73 * (z ** 4)
        s17p = (gap**2) * z * self.k17
        # C8
        o8p = gap * self.k83 * (z**5)
        o18p = (gap**2) * self.k18 * (z**2)
        return {'gap': gap, 'dhap': dhap, 'e4p': e4p, 'x5p': x5p, 'ru5p': ru5p, 'r5p': r5p, 'g6p': g6p, 'f6p': f6p,
                's7p': s7p, 's17p': s17p, 'o8p': o8p, 'o18p': o18p}

    def check_reliability(self):
        res = self.get_all(self.C, self.P)
        new_c = 3 * res['gap'] + 3 * res['dhap'] + 4 * res['e4p'] + 5 * res['x5p'] + 5 * res['ru5p'] + 5 * res['r5p']\
                + 6 * res['g6p'] + 6 * res['f6p'] + 7 * res['s7p'] + 7 * res['s17p'] + 8 * res['o8p'] + 8 * res['o18p']
        new_p = 1 * res['gap'] + 1 * res['dhap'] + 1 * res['e4p'] + 1 * res['x5p'] + 1 * res['ru5p'] + 1 * res['r5p']\
                + 1 * res['g6p'] + 1 * res['f6p'] + 1 * res['s7p'] + 2 * res['s17p'] + 1 * res['o8p'] + 2 * res['o18p']

        if 0.9999*self.C < new_c < 1.0001*self.C and 0.9999*self.P < new_p < 1.0001*self.P:
            print('Calculated C and P within a 0.01% numerical error range')
        else:
            warnings.warn('Numerical Error of calculated P and C bigger than 0.01%.', Warning)

    def metaboliteChange(self, C, P, outputfile, steps=500):
        """Plots all included Species for one constant parameter and a range over the other parameter
        type(C) = float or list
        type(P) = float or list
        """
        colors = {'gap': 'black', 'dhap': 'blue', 'e4p': 'red', 'x5p': 'magenta', 'ru5p': 'brown', 'r5p': 'yellow',
                  'f6p': 'green', 'g6p': 'olive', 's7p': 'orange', 's17p': 'cyan', 'o8p': 'grey', 'o18p': 'indigo'}
        if type(C) is list and type(P) is float:
            self.constraints(C[0], P)
            self.constraints(C[1], P)
            x = np.linspace(C[0], C[1], steps)
            plt.figure(figsize=(11, 6))
            res = {'gap': [], 'dhap': [], 'e4p': [], 'x5p': [], 'ru5p': [], 'r5p': [], 'g6p': [], 'f6p': [], 's7p': [],
                   's17p': [], 'o8p': [], 'o18p': []}
            for i in x:
                tmp_res = self.get_all(i, P)
                for specs in res:
                    res[specs].append(tmp_res[specs])
            counter = 0
            for spec in res:
                plt.plot(x*1000, np.arrray(res[spec])*1000, label=spec, color=colors[spec], linewidth=3)
                counter += 1
            with PdfPages(outputfile) as pdf:
                plt.ylabel(r'Concentration [mmol/l]')
                plt.xlabel(r'C [mmol/l]')
                plt.legend()
                plt.xlim(np.array(C)*1000)
                plt.title('P constant at a level of ' + str(P*1000) + '[mmol/l]')
                pdf.savefig()
                plt.close()

        elif type(P) is list and type(C) is float:
            self.constraints(C, P[0])
            self.constraints(C, P[1])
            x = np.linspace(P[0], P[1], steps)
            plt.figure(figsize=(11, 6))
            res = {'gap': [], 'dhap': [], 'e4p': [], 'x5p': [], 'ru5p': [], 'r5p': [], 'g6p': [], 'f6p': [], 's7p': [],
                   's17p': [], 'o8p': [], 'o18p': []}
            for i in x:
                tmp_res = self.get_all(C, i)
                for specs in res:
                    res[specs].append(tmp_res[specs])
            counter = 0
            for spec in res:
                plt.plot(x*1000, np.array(res[spec])*1000, label=spec, color=colors[spec], linewidth=3)
                counter += 1
            with PdfPages(outputfile) as pdf:
                plt.ylabel(r'Concentration [mmol/l]')
                plt.xlabel(r'P [mmol/l]')
                plt.legend(bbox_to_anchor=(0.3, 1))
                plt.title('C constant at a level of ' + str(C*1000) + ' [mmol/l]')
                plt.xlim(np.array(P)*1000)
                plt.text(244, 250, 'a)', fontsize=20)
                plt.text(294, 250, 'b)', fontsize=20)
                plt.axvline(300, color='black', linestyle='--')
                plt.axvline(250, color='black', linestyle='--')
                pdf.savefig()
                plt.close()
        else:
            raise ValueError('\n\nC and P, one must be of type list, the other of type float\n')

    def adj_concentration(self, datafile, datapoint, unit, sp, factors=(1, 1, 1)):
        """Returns a plot of all adjusted concentrations (sugars with one phosphate group)
         including a linear fit (based on Kartal et al. [1])
        :param datafile: csv file
        :param datapoint: datapoint (from datafile) to use for the plot
        :param unit: Unit of datafile (kcal, kJ od J)
        :param sp: matplotlib subplot to use for the plot
        :return: matplotlib subplot

        [1] Kartal, Önder, et al. "Carbohydrate‐active enzymes exemplify entropic principles in metabolism."
            Molecular systems biology 7.1 (2011): 542.
        """
        from sklearn import linear_model

        if unit == 'kcal':
            self.pre = 4.1868*1000
        elif unit == 'cal':
            self.pre = 4.1868
        elif unit == 'kJ' or unit == 'KJ' or unit == 'kj':
            self.pre = 1000
        elif unit == 'J' or unit == 'j':
            self.pre = 1
        else:
            raise TypeError('No valid Unit')
        self.data = pd.read_csv(datafile)
        self.ac={}
        self.data_trans = {'gap': 'G3P', 'dhap': 'DHAP', 'e4p': 'E4P', 'r5p': 'R5P', 'ru5p': 'Ru5P',
                           'x5p': 'Xylulose 5-phosphate*', 'g6p': 'G6P', 'f6p': 'F6P', 's7p': 'S7P',
                           's17p': 'SBP*', 'o8p': 'O8P*', 'o18p': 'OBP*'}
        for j in self.nu:
            if j == 's17p':
                self.ac[j] = self.data[self.data_trans[j]][datapoint]*factors[0] * exp(-self.nu[j] *
                                                                                       self.pre / (self.R * self.T))
            elif j == 'o8p':
                self.ac[j] = self.data[self.data_trans[j]][datapoint]*factors[1] * exp(-self.nu[j] *
                                                                                       self.pre / (self.R * self.T))
            elif j == 'o18p':
                self.ac[j] = self.data[self.data_trans[j]][datapoint]*factors[2] * exp(-self.nu[j] *
                                                                                       self.pre / (self.R * self.T))
            else:
                self.ac[j] = self.data[self.data_trans[j]][datapoint] * exp(-self.nu[j]*self.pre / (self.R*self.T))

        del self.ac['o18p']
        del self.ac['s17p']
        del self.ac['dhap']

        ac_vec = []
        c_vec = []
        for idx in self.ac:
            c_vec.append(self.dGf[idx][1]+3)
            ac_vec.append(self.ac[idx])
        sort_i = sorted(range(len(c_vec)), key=lambda k: c_vec[k])
        c_vec_s = []
        ac_vec_s = []
        for i in sort_i:
            c_vec_s.append(c_vec[i])
            ac_vec_s.append(ac_vec[i])

        # Linear Regression
        regr = linear_model.LinearRegression()
        # Train the model using the training sets
        regr.fit(np.array([c_vec_s]).transpose(), np.log10(ac_vec_s))

        # Plot the results
        sp.plot(range(4, 9), regr.predict(np.array([range(4, 9)]).transpose()), color='blue', linewidth=3)
        for i in self.ac:
            sp.plot(self.dGf[i][1] + 3, np.log10(self.ac[i]), '.', label=i, markersize=15)
        sp.set_title('datapoint Number :' + str(datapoint))
        sp.legend(numpoints=1, loc='upper left')
        sp.set_ylabel(r'adjusted concentrations $(log_{10})$')
        sp.set_xlim([2.5, 8.5])
        sp.set_xlabel('Number of C-Atoms ')


class DataImport:
    """Accesses data, to read out the concentrations for the metabolites of the Pentose-Phosphate-Pathway
    Datafile must be a csv file, with concentrations for each needed metabolite in the columns.
    self.species must be adapted. It includes the header of the columns in the correct order"""

    def __init__(self, data):
        self.ms_data = pd.read_csv(data)
        self.species = ['G3P', 'DHAP', 'E4P', 'Xylulose 5-phosphate*', 'Ru5P', 'R5P', 'G6P', 'F6P', 'S7P', 'SBP*',
                        'O8P*', 'OBP*']

    def data_access(self, datapoint, factors=(1, 1, 1), conversion=1e-6):
        """Returns: C, P
        C = 3*gap + 3*dhap + 4*e4p+ 5x5p+ 5ru5p+ 5r5p +  6*g6p + 6*f6p+ 7*s7p + 7*s17p + 8*o8p + 8o18p
        P = gap + dhap + e4p + x5p + ru5p + r5p + g6p + f6p + s7p + 2*s17p + o8p + 2*o18p"""

        values = []
        for a in self.species:
            values.append(self.ms_data[a][datapoint] * conversion)
        if pd.isnull(values).any():
            raise MissingDataError('Not all needed data available')
        else:
            C = 3*values[0] + 3*values[1] + 4*values[2] + 5*values[3] + 5*values[3] + 5*values[4] + 5*values[5]\
                + 6*values[6] + 6*values[7] + 7*values[8] + 7*factors[0]*values[9] + 8*factors[1]*values[10] +\
                9*factors[2]*values[11]
            P = sum(values) + factors[0]*values[9] + factors[2]*values[11] - values[10] + factors[1]*values[10]
            return C, P


def model_data_comparison(file, fac=(1, 1, 1), data='data/ms_data_transpose_filled.csv'):
    """Compares the data and the model. For internal use only, since it only working on the used data."""
    ms_data = pd.read_csv(data)
    colors = {'gap': 'black', 'dhap': 'blue', 'e4p': 'red', 'x5p': 'magenta', 'ru5p': 'brown', 'r5p': 'yellow',
              'f6p': 'green', 'g6p': 'olive', 's7p': 'orange', 's17p': 'cyan', 'o8p': 'grey', 'o18p': 'indigo'}
    training = ms_data.shape[0]
    gap = {'data': np.zeros(training), 'model': np.zeros(training)}
    dhap = {'data': np.zeros(training), 'model': np.zeros(training)}
    e4p = {'data': np.zeros(training), 'model': np.zeros(training)}
    x5p = {'data': np.zeros(training), 'model': np.zeros(training)}
    ru5p = {'data': np.zeros(training), 'model': np.zeros(training)}
    r5p = {'data': np.zeros(training), 'model': np.zeros(training)}
    s7p = {'data': np.zeros(training), 'model': np.zeros(training)}
    g6p = {'data': np.zeros(training), 'model': np.zeros(training)}
    f6p = {'data': np.zeros(training), 'model': np.zeros(training)}
    s17p = {'data': np.zeros(training), 'model': np.zeros(training)}
    o8p = {'data': np.zeros(training), 'model': np.zeros(training)}
    o18p = {'data': np.zeros(training), 'model': np.zeros(training)}

    data = DataImport()
    model = PPPmodel()
    for i in range(training):
        try:
            C, P = data.data_access(i, factors=fac, conversion=1e-6)
            C, P = model.constraints(C, P)
            tmp = model.get_all(C, P)

            gap['model'][i] = tmp['gap'] * 1e6
            dhap['model'][i] = tmp['dhap'] * 1e6
            e4p['model'][i] = tmp['e4p'] * 1e6
            x5p['model'][i] = tmp['x5p'] * 1e6
            ru5p['model'][i] = tmp['ru5p'] * 1e6
            r5p['model'][i] = tmp['r5p'] * 1e6
            g6p['model'][i] = tmp['g6p'] * 1e6
            f6p['model'][i] = tmp['f6p'] * 1e6
            s7p['model'][i] = tmp['s7p'] * 1e6
            s17p['model'][i] = tmp['s17p'] * 1e6
            o8p['model'][i] = tmp['o8p'] * 1e6
            o18p['model'][i] = tmp['o18p'] * 1e6

            gap['data'][i] = ms_data['G3P'][i]
            dhap['data'][i] = ms_data['DHAP'][i]
            e4p['data'][i] = ms_data['E4P'][i]
            x5p['data'][i] = ms_data['Xylulose 5-phosphate*'][i]
            ru5p['data'][i] = ms_data['Ru5P'][i]
            r5p['data'][i] = ms_data['R5P'][i]
            s7p['data'][i] = ms_data['S7P'][i]
            g6p['data'][i] = ms_data['G6P'][i]
            f6p['data'][i] = ms_data['F6P'][i]
            s17p['data'][i] = ms_data['SBP*'][i] * fac[0]
            o8p['data'][i] = ms_data['O8P*'][i] * fac[1]
            o18p['data'][i] = ms_data['OBP*'][i] * fac[2]
        except MissingDataError:
            gap['data'][i] = ms_data['G3P'][35]
            dhap['data'][i] = ms_data['G3P'][35]
            e4p['data'][i] = ms_data['G3P'][35]
            x5p['data'][i] = ms_data['G3P'][35]
            ru5p['data'][i] = ms_data['G3P'][35]
            r5p['data'][i] = ms_data['G3P'][35]
            s7p['data'][i] = ms_data['G3P'][35]
            f6p['data'][i] = ms_data['G3P'][35]
            s17p['data'][i] = ms_data['G3P'][35]
            g6p['data'][i] = ms_data['G3P'][35]
            o8p['data'][i] = ms_data['G3P'][35]
            o18p['data'][i] = ms_data['G3P'][35]

            gap['model'][i] = ms_data['G3P'][35]
            dhap['model'][i] = ms_data['G3P'][35]
            e4p['model'][i] = ms_data['G3P'][35]
            x5p['model'][i] = ms_data['G3P'][35]
            ru5p['model'][i] = ms_data['G3P'][35]
            r5p['model'][i] = ms_data['G3P'][35]
            s7p['model'][i] = ms_data['G3P'][35]
            f6p['model'][i] = ms_data['G3P'][35]
            s17p['model'][i] = ms_data['G3P'][35]
            g6p['model'][i] = ms_data['G3P'][35]
            o8p['model'][i] = ms_data['G3P'][35]
            o18p['model'][i] = ms_data['G3P'][35]
    with PdfPages(file) as pdf:
        fig = plt.figure(figsize=(10, 13))
        ax1 = fig.add_subplot(4, 1, 1)
        ax2 = fig.add_subplot(4, 1, 2)
        ax3 = fig.add_subplot(4, 1, 3)
        ax4 = fig.add_subplot(4, 1, 4)
        ax1.plot(ms_data['Time'], gap['data'], 'o-', color=colors['gap'], label='GAP (Data)')
        ax1.plot(ms_data['Time'], gap['model'], '.--', color=colors['gap'], label='GAP (Model)')
        ax1.plot(ms_data['Time'], e4p['data'], 'o-', color=colors['e4p'], label='E4P (Data)')
        ax1.plot(ms_data['Time'], e4p['model'], '.--', color=colors['e4p'], label='E4P (Model)')
        ax1.plot(ms_data['Time'], dhap['data'], 'o-', color=colors['dhap'], label='DHAP (Data)')
        ax1.plot(ms_data['Time'], dhap['model'], '.--', color=colors['dhap'], label='DHAP (Model)')

        ax2.plot(ms_data['Time'], x5p['data'], 'o-', color=colors['x5p'], label='X5P (Data)')
        ax2.plot(ms_data['Time'], x5p['model'], '.--', color=colors['x5p'], label='X5P (Model)')
        ax2.plot(ms_data['Time'], ru5p['data'], 'o-', color=colors['ru5p'], label='Ru5P (Data)')
        ax2.plot(ms_data['Time'], ru5p['model'], '.--', color=colors['ru5p'], label='Ru5P (Model)')
        ax2.plot(ms_data['Time'], r5p['data'], 'o-', color=colors['r5p'], label='R5P (Data)')
        ax2.plot(ms_data['Time'], r5p['model'], '.--', color=colors['r5p'], label='R5P (Model)')
        ax2.plot(ms_data['Time'], s17p['data'], 'o-', color=colors['s17p'], label='S17P (data)')
        ax2.plot(ms_data['Time'], s17p['model'], '.--', color=colors['s17p'], label='S17P (Model)')
        ax4.plot(ms_data['Time'], o18p['data'], 'o-', color=colors['o18p'], label='O18P (data)')
        ax4.plot(ms_data['Time'], o18p['model'], '.--', color=colors['o18p'], label='O18P (Model)')
        ax4.plot(ms_data['Time'], o8p['data'], 'o-', color=colors['o8p'], label='O8P (Data)')
        ax4.plot(ms_data['Time'], o8p['model'], '.--', color=colors['o8p'], label='O8P (Model)')

        ax3.plot(ms_data['Time'], s7p['data'], 'o-', color=colors['s7p'], label='S7P (Data)')
        ax3.plot(ms_data['Time'], s7p['model'], '.--', color=colors['s7p'], label='S7P (Model)')
        ax3.plot(ms_data['Time'], f6p['data'], 'o-', color=colors['f6p'], label='F6P (Data)')
        ax3.plot(ms_data['Time'], f6p['model'], '.--', color=colors['f6p'], label='F6P (Model)')
        ax3.plot(ms_data['Time'], g6p['data'], 'o-', color=colors['g6p'], label='G6P (Data)')
        ax3.plot(ms_data['Time'], g6p['model'], '.--', color=colors['g6p'], label='G6P (Model)')

        legends=(1.1, 1.0)
        fs=11
        ax1.legend(bbox_to_anchor=legends, fontsize=fs)
        # ax1.set_xlabel(r'$Time \ [s]$')
        ax1.set_ylabel(r'$Concentration \ [\mu M]$')
        ax2.legend(bbox_to_anchor=legends, fontsize=fs)
        # ax2.set_xlabel(r'$Time \ [s]$')
        ax2.set_ylabel(r'$Concentration \ [\mu M]$')
        ax3.legend(bbox_to_anchor=legends, fontsize=fs)
        # ax3.set_xlabel(r'$Time \ [s]$')
        ax3.set_ylabel(r'$Concentration \ [\mu M]$')
        ax4.legend(bbox_to_anchor=legends, fontsize=fs)
        ax4.set_ylabel(r'$Concentration \ [\mu M]$')
        ax4.set_xlabel(r'$Time \ [s]$')
        pdf.savefig()
        plt.close()


def main():
    model = PPPmodel()
    model.get_all(0.1, 0.03)
    model.check_reliability()
    model.metaboliteChange(1., [0.15, 0.33], 'mc.pdf')

if __name__ == '__main__':
    main()
