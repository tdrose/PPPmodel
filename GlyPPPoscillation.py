#!/usr/bin/python3

from ppp_model_complete import *
from scipy.integrate import ode
import matplotlib.pyplot as plt
from math import sin, pi

class GlyPPPmodelosc(PPPmodel):
    """
    Connbined model of the glycolysis and the Pentose Phosphate Pathway.
    Kintetic laws are proposed by Messiha et al. [1]. The kinetic law of the SHB17 iy published by Clasquin et al. [2]
    The non-oxidative Pentose Phosphate Pathway is replaced by the EQ-Module.

    Oscillations are simulated with out of phase oscillations of the enzymes SHB17 and ZWF1..

    [1] Messiha, Hanan L., et al. "Enzyme characterisation and kinetic modelling of the pentose phosphate pathway in yeast."
        PeerJ 2.e146v4 (2014).
    [2] Clasquin, Michelle F., et al. "Riboneogenesis in yeast." Cell 145.6 (2011): 969-980.
    """
    def __init__(self):
        super().__init__()
        self.params = {'RKI_kcat': 335.0, 'ZWF_Knadph': 0.017, 'SOL_Kp6g': 0.5, 'PGM_Keq': 0.1667,
                       'ENO_Kp2g_ENO1': 0.043, 'GND_kcat_GND1': 28.0, 'PDC_Kpyr_PDC1': 8.5, 'HXK_Kg6p_HXK2': 30.0,
                       'PGK_Kp3g': 4.58, 'PFK_Kadp': 1.0, 'TPI_Kdhap': 6.454, 'PGM_Kg6p': 0.05,
                       'TPP_Vmax': 2.33999999999999, 'PGK_Kbpg': 0.003, 'FBA_Kf16bp': 0.4507, 'sum_NADP': 0.33,
                       'NADPH_oxidase_k': 1.0, 'HXK_Kglc_HXK1': 0.15, 'UGP_Kiutp': 0.11, 'E4P_sink_k': 1.0,
                       'TPS_Kg6p': 3.8, 'TKL__R5PS7P__kcat': 40.5, 'GPD_Keq': 10000.0, 'HXK_Katp_GLK1': 0.865,
                       'ATPase_Vmax': 6.16, 'TAL_Ke4p_TAL1': 0.362, 'ENO_Kp2g_ENO2': 0.104, 'PDC_Kpyr_PDC6': 2.92,
                       'PFK_Kiatp': 0.65, 'HXT_Ki': 0.91, 'HXK_Kit6p_HXK1': 0.2, 'HXK_kcat_HXK2': 63.1,
                       'PDC_kcat_PDC5': 10.32, 'TDH_Knad_TDH3': 0.09, 'GND_Kru5p_GND2': 0.1, 'ZWF_kcat': 189.0,
                       'PFK_Cf16': 0.397, 'TPI_Kgap': 5.25, 'TKL__R5PS7P__Keq': 1.2, 'TDH_Kbpg_TDH1': 0.0098,
                       'PFK_kcat': 209.6, 'GND_Kp6g_GND2': 0.115, 'PYK_L0': 100.0, 'GND_Kp6g_GND1': 0.062,
                       'PGI_Keq': 0.29, 'ENO_kcat_ENO1': 7.6, 'TPP_Kt6p': 0.5, 'PFK_Kamp': 0.0995, 'GPD_Knad': 0.93,
                       'UGP_Vmax': 13.2552, 'RKI_Kr5p': 5.7, 'HXK_Kg6p_GLK1': 30.0, 'udp_to_utp_k': 0.0745258294103764,
                       'TKL__E4PF6P__kcat': 47.1, 'ADH_Kinad': 0.92, 'TDH_Knadh_TDH3': 0.06, 'HXK_Kglc_HXK2': 0.2,
                       'GPD_Knadh': 0.023, 'GPD_Katp': 0.73, 'Ke4p_TAL': 0.946, 'GPM_Keq': 0.19, 'PGK_kcat': 58.6,
                       'TDH_Keq': 0.00533412710224736, 'PGK_Kadp': 0.2, 'FBA_Keq': 0.069, 'PFK_Catp': 3.0,
                       'HXT_Vmax': 3.35, 'PDC_kcat_PDC6': 9.21, 'PGI_kcat': 487.36, 'PGK_Keq': 3200.0,
                       'TAL_kcat_NQM1': 0.694, 'sum_AxP': 6.02, 'PYK_Keq': 6500.0, 'RKI_Kru5p': 2.47,
                       'HXK_kcat_GLK1': 0.0721, 'TAL_Ke4p_NQM1': 0.305, 'ADH_Ketoh': 17.0, 'GPD_Kadp': 2.0,
                       'PYK_Kpep': 0.281, 'HXK_Kadp_GLK1': 0.23, 'PGK_nHadp': 2.0, 'ENO_Keq': 6.7,
                       'GND_Kru5p_GND1': 0.1, 'HXK_Kadp_HXK1': 0.23, 'sum_NAD': 1.59, 'GND_Knadp_GND1': 0.094,
                       'ADH_Kiacald': 1.1, 'TPI_Kigap': 35.1, 'HXT_Kglc': 0.9, 'TPS_Kudg': 0.886, 'AK_Keq': 0.45,
                       'TAL_kcat_TAL1': 0.694, 'TDH_Kgap_TDH3': 0.423, 'TDH_Knad_TDH1': 0.09, 'TAL_Kgap_TAL1': 0.272,
                       'AK_k': 0.75, 'PFK_L0': 0.66, 'TPI_kcat': 564.38, 'HXK_kcat_HXK1': 10.2, 'RPE_kcat': 4020.0,
                       'GPD_Kg3p': 1.2, 'Kr5p_TAL': 0.235, 'GPD_Kfbp': 4.8, 'TAL_Kf6p_NQM1': 1.04,
                       'TDH_Knadh_TDH1': 0.06, 'Ks7p_TAL': 0.15, 'ADH_Knadh': 0.11, 'GPD_Kdhap': 0.54,
                       'TAL_Kgap_NQM1': 0.272, 'PGI_Kg6p': 1.0257, 'ADH_kcat': 176.0, 'ENO_Kpep_ENO1': 0.5,
                       'FBA_Kgap': 2.4, 'PYK_Kf16p': 0.2, 'PFK_Camp': 0.0845, 'PGM_Vmax': 0.12762, 'PFK_Keq': 800.0,
                       'GPP_Vmax': 0.883333333333333, 'HXK_Kg6p_HXK1': 30.0, 'GND_Knadph_GND1': 0.055, 'PFK_Kf6p': 0.1,
                       'ADH_Knad': 0.17, 'PDC_kcat_PDC1': 12.14, 'HXK_Katp_HXK2': 0.195, 'UGP_Kutp': 0.11,
                       'TDH_Kgap_TDH1': 0.495, 'ZWF_Kg6l': 0.01, 'TPI_Keq': 0.045,
                       'acetate_branch_k': 0.00554339592436782, 'ENO_kcat_ENO2': 19.87, 'GPD_Vmax': 0.783333333333333,
                       'GPP_Kg3p': 3.5, 'RPE_Keq': 1.4, 'ADH_Kacald': 0.4622, 'PFK_Kf26': 0.000682, 'R5P_sink_k': 1.0,
                       'HXK_Kit6p_HXK2': 0.04, 'TKL__E4PF6P__Keq': 10.0, 'PFK_Katp': 0.71, 'ATPase_Katp': 3.0,
                       'RPE_Kx5p': 7.7, 'TDH_kcat_TDH3': 18.162, 'TAL_Ks7p_NQM1': 0.786, 'RKI_Keq': 4.0,
                       'GPM_Kp2g': 1.41, 'PYK_Katp': 1.5, 'GPM_kcat': 400.0, 'PGM_Kg1p': 0.023, 'FBA_Kigap': 10.0,
                       'ADH_Keq': 14492.7536231884, 'UGP_Kg1p': 0.32, 'TDH_Kbpg_TDH3': 0.909, 'PYK_Kiatp': 9.3,
                       'sum_UxP': 1.39784619487425, 'ADH_Kietoh': 90.0, 'HXK_Keq': 2000.0, 'GND_Knadph_GND2': 0.055,
                       'PYK_kcat': 20.146, 'GND_kcat_GND2': 27.3, 'ZWF_Kg6p': 0.042, 'HXK_Kadp_HXK2': 0.23,
                       'SOL_Kg6l': 0.8, 'PFK_Cf26': 0.0174, 'FBA_kcat': 4.139, 'RPE_Kru5p': 5.97, 'GPM_Kp3g': 1.2,
                       'PGK_Katp': 1.99, 'Kx5p_TAL': 0.67, 'Kf6p_TAL': 1.1, 'PYK_Kpyr': 21.0, 'TDH_kcat_TDH1': 19.12,
                       'ADH_Kinadh': 0.031, 'Kgap_TAL': 0.1, 'RKI_Kiru5p': 9.88, 'SOL_kcat': 4.3, 'UGP_Kiudg': 0.0035,
                       'ZWF_Knadp': 0.045, 'HXK_Katp_HXK1': 0.293, 'TAL_Ks7p_TAL1': 0.786, 'PGI_Kf6p': 0.307,
                       'PYK_Kadp': 0.243, 'GND_Knadp_GND2': 0.094, 'FBA_Kdhap': 2.0, 'TAL_Kf6p_TAL1': 1.44,
                       'PFK_Kf16': 0.111, 'HXK_Kglc_GLK1': 0.0106, 'ENO_Kpep_ENO2': 0.5, 'PFK_gR': 5.12,
                       'TAL_Keq': 1.05, 'TPS_Vmax': 0.49356, 'PFK_Ciatp': 100.0, 'PDC_Kpyr_PDC5': 7.08}
        self.shbparams = {'vm': 1, 'kms': 0.034, 'kmf': 0.51}  # 'vm': 0.4347*0.0004546

        # From the initial Conentrations of Messiha 2014 [1] (without S17P, O8P, O18P)
        C = 3 * 1.1613768527467 + 3 * 0.315891028770503 + 4 * 0.029 + 5 * 0.041 + 5 * 0.033 + 5 * 0.118 + \
            6 * 0.772483203645216 + 6 * 0.235441221891221 + 7 * 0.082
        P = 1 * 1.1613768527467 + 1 * 0.315891028770503 + 1 * 0.029 + 1 * 0.041 + 1 * 0.033 + 1 * 0.118 + \
            1 * 0.772483203645216 + 1 * 0.235441221891221 + 1 * 0.082

        self.init_arguments = [1.29, 4.29, 0.178140579850657, 0.000736873499865602, 4.58321859006931, 0.539248506344921,
                               0.274002929191284, 6.28000179338242, 1.50329030201531, 0.0677379081099344,
                               0.469825011134444, 0.610005413358042, 2.10847140717419, 0.02, 0.2815, 0.6491,
                               0.44, 0.0867096979846952, 0.467246194874247, 0.1, 0.16, 0.25, 0.17, C, P]
        self.glycolysis_ppp_species = ['ADP', 'ATP', 'AcAld', 'BPG', 'DHAP', 'F16bP', 'F6P', 'G1P', 'G3P', 'G6P',
                                       'GAP', 'GLC', 'NAD', 'P2G', 'P3G', 'PEP', 'PYR', 'T6P', 'UDP', 'UTP', 'AMP',
                                       'NADH', 'UDG', 'ACE', 'EtOH', 'F26bP', 'GLCx', 'GLY', 'SUC', 'TRH', 'ADH1',
                                       'CDC19', 'ENO1', 'ENO2', 'FBA1', 'GLK1', 'GPD1', 'GPD2', 'GPM1', 'HOR2', 'HXK1',
                                       'HXK2', 'PDC1', 'PDC5', 'PDC6', 'PFK1', 'PFK2', 'PGI1', 'PGK1', 'PGM1', 'PGM2',
                                       'RHR2', 'TDH1', 'TDH3', 'TPI1', 'TPS1', 'TPS2', 'UGP1', 'E4P', 'G6L', 'NADPH',
                                       'P6G', 'R5P', 'Ru5P', 'S7P', 'X5P', 'NADP', 'GND1', 'GND2', 'NQM1', 'RKI1',
                                       'RPE1', 'SOL3', 'TAL1', 'TKL1', 'ZWF1']

        # Initial Parameters from the data provided by Dr. Douglas Murray.

        self.init_arg_data = [0.9344, 0.915, 0.018, 0.0003, 0.2374, 0.1815, 0.003, 6, 0.606, 0.040, 0.006, 0.143, 0.189,
                              0.089, 0.1378, 0.2843, 0.2057, 0.06, 0.4027, 0.03, 0.280, 0.0733, 0.288,
                              0.0038593227282255997 * 1000, 0.00062188528954060002 * 1000]

        self.ACE = 223.000253398294
        self.EtOH = 221.890311417536
        self.F26bP = 0.003
        self.GLCx = 74.0
        self.GLY = 0.15
        self.SUC = 0.0
        self.TRH = 0.0153878853696526
        self.ADH1 = 0.163908510928009
        self.CDC19 = 2.0483901071712
        self.ENO1 = 0.686371954155832
        self.ENO2 = 1.97444629317817
        self.FBA1 = 1.33839465704882
        self.GLK1 = 0.0450869624419226
        self.GPD1 = 0.00683511177089872
        self.GPD2 = 0.000793405666424228
        self.GPM1 = 0.73000029889707
        self.HOR2 = 0.0054734695639756
        self.HXK1 = 0.0167807457149784
        self.HXK2 = 0.0613313539705155
        self.PDC1 = 1.06781077822834
        self.PDC5 = 0.0123547443267676
        self.PDC6 = 0.00654086421106118
        self.PFK1 = 0.0467850299063124
        self.PFK2 = 0.0390366215332091 * 0.03
        self.PGI1 = 0.1382907072901
        self.PGK1 = 0.257656912658955
        self.PGM1 = 0.00326229546307459
        self.PGM2 = 0.00125868877176552
        self.RHR2 = 0.0511804773718313
        self.TDH1 = 0.350864642801396
        self.TDH3 = 4.20440474648547
        self.TPI1 = 0.294357819645508
        self.TPS1 = 0.00339248174237065
        self.TPS2 = 0.00265985181347494
        self.UGP1 = 0.00620211419860714
        self.GND1 = 0.013
        self.GND2 = 0.003
        self.NQM1 = 0.02
        self.RKI1 = 0.05
        self.RPE1 = 0.03
        self.SOL3 = 0.0296
        self.TAL1 = 0.144
        self.TKL1 = 0.455
        self.ZWF1 = 0.02

    @staticmethod
    def oscillation(t, phaseshift=0, ampFac=1.):
        return 0.45 * ampFac * sin((2 * pi / 3500) * t + phaseshift) + 1

    def ADH_func(self, AcAld, NAD, NADH, cell=1):
        out = cell * (self.ADH1 * self.params['ADH_kcat'] * (AcAld * NADH / (self.params['ADH_Kacald'] * self.params['ADH_Kinadh']) - self.EtOH * NAD / (self.params['ADH_Kacald'] * self.params['ADH_Kinadh'] * self.params['ADH_Keq'])) / (1 + NADH / self.params['ADH_Kinadh'] + AcAld * self.params['ADH_Knadh'] / (self.params['ADH_Kinadh'] * self.params['ADH_Kacald']) + self.EtOH * self.params['ADH_Knad'] / (self.params['ADH_Kinad'] * self.params['ADH_Ketoh']) + NAD / self.params['ADH_Kinad'] + AcAld * NADH / (self.params['ADH_Kinadh'] * self.params['ADH_Kacald']) + NADH * self.EtOH * self.params['ADH_Knad'] / (self.params['ADH_Kinadh'] * self.params['ADH_Kinad'] * self.params['ADH_Ketoh']) + AcAld * NAD * self.params['ADH_Knadh'] / (self.params['ADH_Kinadh'] * self.params['ADH_Kinad'] * self.params['ADH_Kacald']) + self.EtOH * NAD / (self.params['ADH_Ketoh'] * self.params['ADH_Kinad']) + AcAld * NADH * self.EtOH / (self.params['ADH_Kinadh'] * self.params['ADH_Kietoh'] * self.params['ADH_Kacald']) + AcAld * self.EtOH * NAD / (self.params['ADH_Kiacald'] * self.params['ADH_Kinad'] * self.params['ADH_Ketoh'])))
        return out

    def AK_func(self, ADP, ATP, AMP, cell=1):
        out = cell * self.params['AK_k'] * (ADP * ADP - AMP * ATP / self.params['AK_Keq'])
        return out

    def ATPase_func(self, ATP, cell=1):
        out = cell * (self.params['ATPase_Vmax'] * ATP / self.params['ATPase_Katp'] / (1 + ATP / self.params['ATPase_Katp']))
        return out

    def ENO_func(self, P2G, PEP, cell=1):
        out = cell * (self.ENO1 * self.params['ENO_kcat_ENO1'] * (P2G / self.params['ENO_Kp2g_ENO1'] - PEP / (self.params['ENO_Kp2g_ENO1'] * self.params['ENO_Keq'])) / (1 + P2G / self.params['ENO_Kp2g_ENO1'] + PEP / self.params['ENO_Kpep_ENO1'])) + cell * (self.ENO2 * self.params['ENO_kcat_ENO2'] * (P2G / self.params['ENO_Kp2g_ENO2'] - PEP / (self.params['ENO_Kp2g_ENO2'] * self.params['ENO_Keq'])) / (1 + P2G / self.params['ENO_Kp2g_ENO2'] + PEP / self.params['ENO_Kpep_ENO2']))
        return out

    def FBA_func(self, DHAP, F16bP, GAP, cell=1):
        out = cell * (self.FBA1 * self.params['FBA_kcat'] * (F16bP / self.params['FBA_Kf16bp'] - DHAP * GAP / (self.params['FBA_Kf16bp'] * self.params['FBA_Keq'])) / (1 + F16bP / self.params['FBA_Kf16bp'] + DHAP / self.params['FBA_Kdhap'] + GAP / self.params['FBA_Kgap'] + F16bP * GAP / (self.params['FBA_Kf16bp'] * self.params['FBA_Kigap']) + DHAP * GAP / (self.params['FBA_Kdhap'] * self.params['FBA_Kgap'])))
        return out

    def GPD_func(self, ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=1):
        out = cell * (self.params['GPD_Vmax'] / (self.params['GPD_Kdhap'] * self.params['GPD_Knadh']) * (DHAP * NADH - G3P * NAD / self.params['GPD_Keq']) / ((1 + F16bP / self.params['GPD_Kfbp'] + ATP / self.params['GPD_Katp'] + ADP / self.params['GPD_Kadp']) * (1 + DHAP / self.params['GPD_Kdhap'] + G3P / self.params['GPD_Kg3p']) * (1 + NADH / self.params['GPD_Knadh'] + NAD / self.params['GPD_Knad'])))
        return out

    def GPM_func(self, P2G, P3G, cell=1):
        out = cell * (self.GPM1 * self.params['GPM_kcat'] * (P3G / self.params['GPM_Kp3g'] - P2G / (self.params['GPM_Kp3g'] * self.params['GPM_Keq'])) / (1 + P3G / self.params['GPM_Kp3g'] + P2G / self.params['GPM_Kp2g']))
        return out

    def GPP_func(self, G3P, cell=1):
        out = cell * (self.params['GPP_Vmax'] * G3P / self.params['GPP_Kg3p'] / (1 + G3P / self.params['GPP_Kg3p']))
        return out

    def HXK_func(self, ADP, ATP, G6P, GLC, T6P, cell=1):
        out = cell * (self.HXK1 * self.params['HXK_kcat_HXK1'] * (GLC * ATP / (self.params['HXK_Kglc_HXK1'] * self.params['HXK_Katp_HXK1']) - G6P * ADP / (self.params['HXK_Kglc_HXK1'] * self.params['HXK_Katp_HXK1'] * self.params['HXK_Keq'])) / ((1 + GLC / self.params['HXK_Kglc_HXK1'] + G6P / self.params['HXK_Kg6p_HXK1'] + T6P / self.params['HXK_Kit6p_HXK1']) * (1 + ATP / self.params['HXK_Katp_HXK1'] + ADP / self.params['HXK_Kadp_HXK1']))) + cell * (self.HXK2 * self.params['HXK_kcat_HXK2'] * (GLC * ATP / (self.params['HXK_Kglc_HXK2'] * self.params['HXK_Katp_HXK2']) - G6P * ADP / (self.params['HXK_Kglc_HXK2'] * self.params['HXK_Katp_HXK2'] * self.params['HXK_Keq'])) / ((1 + GLC / self.params['HXK_Kglc_HXK2'] + G6P / self.params['HXK_Kg6p_HXK2'] + T6P / self.params['HXK_Kit6p_HXK2']) * (1 + ATP / self.params['HXK_Katp_HXK2'] + ADP / self.params['HXK_Kadp_HXK2']))) + cell * (self.GLK1 * self.params['HXK_kcat_GLK1'] * (GLC * ATP / (self.params['HXK_Kglc_GLK1'] * self.params['HXK_Katp_GLK1']) - G6P * ADP / (self.params['HXK_Kglc_GLK1'] * self.params['HXK_Katp_GLK1'] * self.params['HXK_Keq'])) / ((1 + GLC / self.params['HXK_Kglc_GLK1'] + G6P / self.params['HXK_Kg6p_GLK1']) * (1 + ATP / self.params['HXK_Katp_GLK1'] + ADP / self.params['HXK_Kadp_GLK1'])))
        return out

    def PDC_func(self, PYR, cell=1):
        out = cell * (self.PDC1 * self.params['PDC_kcat_PDC1'] * PYR / self.params['PDC_Kpyr_PDC1'] / (1 + PYR / self.params['PDC_Kpyr_PDC1'])) + cell * (self.PDC5 * self.params['PDC_kcat_PDC5'] * PYR / self.params['PDC_Kpyr_PDC5'] / (1 + PYR / self.params['PDC_Kpyr_PDC5'])) + cell * (self.PDC6 * self.params['PDC_kcat_PDC6'] * PYR / self.params['PDC_Kpyr_PDC6'] / (1 + PYR / self.params['PDC_Kpyr_PDC6']))
        return out

    def PFK_func(self, ADP, ATP, F16bP, F6P, AMP, cell=1):
        out = cell * self.PFK2 * self.params['PFK_kcat'] * (self.params['PFK_gR'] * (F6P / self.params['PFK_Kf6p']) * (ATP / self.params['PFK_Katp']) * (1 - F16bP * ADP / (F6P * ATP) / self.params['PFK_Keq']) * (1 + F6P / self.params['PFK_Kf6p'] + ATP / self.params['PFK_Katp'] + self.params['PFK_gR'] * F6P / self.params['PFK_Kf6p'] * ATP / self.params['PFK_Katp'] + F16bP / self.params['PFK_Kf16'] + ADP / self.params['PFK_Kadp'] + self.params['PFK_gR'] * F16bP / self.params['PFK_Kf16'] * ADP / self.params['PFK_Kadp']) / (((1 + F6P / self.params['PFK_Kf6p'] + ATP / self.params['PFK_Katp'] + self.params['PFK_gR'] * F6P / self.params['PFK_Kf6p'] * ATP / self.params['PFK_Katp'] + F16bP / self.params['PFK_Kf16'] + ADP / self.params['PFK_Kadp'] + self.params['PFK_gR'] * F16bP / self.params['PFK_Kf16'] * ADP / self.params['PFK_Kadp'])**( 2)) + self.params['PFK_L0'] * (((1 + self.params['PFK_Ciatp'] * ATP / self.params['PFK_Kiatp']) / (1 + ATP / self.params['PFK_Kiatp']))**( 2)) * (((1 + self.params['PFK_Camp'] * AMP / self.params['PFK_Kamp']) / (1 + AMP / self.params['PFK_Kamp']))**( 2)) * (((1 + self.params['PFK_Cf26'] * self.F26bP / self.params['PFK_Kf26'] + self.params['PFK_Cf16'] * F16bP / self.params['PFK_Kf16']) / (1 + self.F26bP / self.params['PFK_Kf26'] + F16bP / self.params['PFK_Kf16']))**( 2)) * ((1 + self.params['PFK_Catp'] * ATP / self.params['PFK_Katp'])**( 2))))
        return out

    def PGK_func(self, ADP, ATP, BPG, P3G, cell=1):
        out = cell * (self.PGK1 * self.params['PGK_kcat'] * ((ADP / self.params['PGK_Kadp'])**( self.params['PGK_nHadp'] - 1)) * (BPG * ADP / (self.params['PGK_Kbpg'] * self.params['PGK_Kadp']) - P3G * ATP / (self.params['PGK_Kbpg'] * self.params['PGK_Kadp'] * self.params['PGK_Keq'])) / ((1 + BPG / self.params['PGK_Kbpg'] + P3G / self.params['PGK_Kp3g']) * (1 + ((ADP / self.params['PGK_Kadp'])**( self.params['PGK_nHadp'])) + ATP / self.params['PGK_Katp'])))
        return out

    def PGM_func(self, G1P, G6P, cell=1):
        out = cell * (self.params['PGM_Vmax'] * (G6P / self.params['PGM_Kg6p'] - G1P / (self.params['PGM_Kg6p'] * self.params['PGM_Keq'])) / (1 + G6P / self.params['PGM_Kg6p'] + G1P / self.params['PGM_Kg1p']))
        return out

    def PYK_func(self, ADP, ATP, F16bP, PEP, PYR, cell=1):
        out = cell * (self.CDC19 * self.params['PYK_kcat'] * (PEP * ADP - PYR * ATP / self.params['PYK_Keq']) / (self.params['PYK_Kpep'] * self.params['PYK_Kadp']) / ((1 + PEP / self.params['PYK_Kpep'] + PYR / self.params['PYK_Kpyr'] + self.params['PYK_L0'] * ((ATP / self.params['PYK_Kiatp'] + 1) / (F16bP / self.params['PYK_Kf16p'] + 1))) * (1 + ADP / self.params['PYK_Kadp'] + ATP / self.params['PYK_Katp'])))
        return out

    def TDH_func(self, BPG, GAP, NAD, NADH, cell=1):
        out = cell * (self.TDH1 * self.params['TDH_kcat_TDH1'] * (GAP * NAD / (self.params['TDH_Kgap_TDH1'] * self.params['TDH_Knad_TDH1']) - BPG * NADH / (self.params['TDH_Kgap_TDH1'] * self.params['TDH_Knad_TDH1'] * self.params['TDH_Keq'])) / ((1 + GAP / self.params['TDH_Kgap_TDH1'] + BPG / self.params['TDH_Kbpg_TDH1']) * (1 + NAD / self.params['TDH_Knad_TDH1'] + NADH / self.params['TDH_Knadh_TDH1']))) + cell * (self.TDH3 * self.params['TDH_kcat_TDH3'] * (GAP * NAD / (self.params['TDH_Kgap_TDH3'] * self.params['TDH_Knad_TDH3']) - BPG * NADH / (self.params['TDH_Kgap_TDH3'] * self.params['TDH_Knad_TDH3'] * self.params['TDH_Keq'])) / ((1 + GAP / self.params['TDH_Kgap_TDH3'] + BPG / self.params['TDH_Kbpg_TDH3']) * (1 + NAD / self.params['TDH_Knad_TDH3'] + NADH / self.params['TDH_Knadh_TDH3'])))
        return out

    def TPP_func(self, T6P, cell=1):
        out = cell * (self.params['TPP_Vmax'] * T6P / self.params['TPP_Kt6p'] / (1 + T6P / self.params['TPP_Kt6p']))
        return out

    def TPS_func(self, G6P, UDG, cell=1):
        out = cell * (self.params['TPS_Vmax'] * G6P * UDG / (self.params['TPS_Kg6p'] * self.params['TPS_Kudg']) / ((1 + G6P / self.params['TPS_Kg6p']) * (1 + UDG / self.params['TPS_Kudg'])))
        return out

    def UGP_func(self, G1P, UTP, UDG, cell=1):
        out = cell * (self.params['UGP_Vmax'] * UTP * G1P / (self.params['UGP_Kutp'] * self.params['UGP_Kg1p']) / (self.params['UGP_Kiutp'] / self.params['UGP_Kutp'] + UTP / self.params['UGP_Kutp'] + G1P / self.params['UGP_Kg1p'] + UTP * G1P / (self.params['UGP_Kutp'] * self.params['UGP_Kg1p']) + self.params['UGP_Kiutp'] / self.params['UGP_Kutp'] * UDG / self.params['UGP_Kiudg'] + G1P * UDG / (self.params['UGP_Kg1p'] * self.params['UGP_Kiudg'])))
        return out

    def acetate_branch_func(self, AcAld, NAD, cell=1):
        out = cell * self.params['acetate_branch_k'] * AcAld * NAD
        return out

    def udp_to_utp_func(self, ATP, UDP, cell=1):
        out = cell * self.params['udp_to_utp_k'] * UDP * ATP
        return out

    def HXT_func(self, GLC, cell=1):
        out = cell * (self.params['HXT_Vmax'] * (self.GLCx - GLC) / self.params['HXT_Kglc'] / (1 + self.GLCx / self.params['HXT_Kglc'] + GLC / self.params['HXT_Kglc'] + self.params['HXT_Ki'] * self.GLCx / self.params['HXT_Kglc'] * GLC / self.params['HXT_Kglc']))
        return out

    def GND_func(self, NADPH, P6G, Ru5P, NADP, cell=1):
        out = cell * (self.GND1 * self.params['GND_kcat_GND1'] * P6G * NADP / (self.params['GND_Kp6g_GND1'] * self.params['GND_Knadp_GND1']) / ((1 + P6G / self.params['GND_Kp6g_GND1'] + Ru5P / self.params['GND_Kru5p_GND1']) * (1 + NADP / self.params['GND_Knadp_GND1'] + NADPH / self.params['GND_Knadph_GND1'])) + self.GND2 * self.params['GND_kcat_GND2'] * P6G * NADP / ((1 + P6G / self.params['GND_Kp6g_GND2'] + Ru5P / self.params['GND_Kru5p_GND2']) * (1 + NADP / self.params['GND_Knadp_GND2'] + NADPH / self.params['GND_Knadph_GND2'])))
        return out

    def SOL_func(self, G6L, P6G, cell=1):
        out = cell * self.SOL3 * self.params['SOL_kcat'] * G6L / self.params['SOL_Kg6l'] / (1 + G6L / self.params['SOL_Kg6l'] + P6G / self.params['SOL_Kp6g'])
        return out

    def ZWF_func(self, G6P, G6L, NADPH, NADP, t, cell=1):
        out = self.oscillation(t, phaseshift=2, ampFac=1.) * cell * self.ZWF1 * self.params['ZWF_kcat'] * G6P * NADP / (self.params['ZWF_Kg6p'] * self.params['ZWF_Knadp']) / ((1 + G6P / self.params['ZWF_Kg6p'] + G6L / self.params['ZWF_Kg6l']) * (1 + NADP / self.params['ZWF_Knadp'] + NADPH / self.params['ZWF_Knadph']))
        return out

    def NADPH_oxidase_func(self, NADPH, cell=1):
        out = cell * self.params['NADPH_oxidase_k'] * NADPH
        return out

    def E4P_sink_func(self, E4P, cell=1):
        out = cell * self.params['E4P_sink_k'] * E4P
        return out

    def R5P_sink_func(self, R5P, cell=1):
        out = cell * self.params['R5P_sink_k'] * R5P
        return out

    def SHB17(self, S17P, F16bP, t):
        out = self.oscillation(t, phaseshift=pi+2, ampFac=1.) * self.shbparams['vm']*S17P / (self.shbparams['kms'] + S17P)
        return out / (1 + (S17P/self.shbparams['kms']) + (F16bP/self.shbparams['kmf']))

    def glycolysis_ppp_system_deriv(self, t, y, cell=1):

        """glycolysis_ppp_system_deriv(arguments, params=None, cell=1 )
                y:	Initial Values for all species in the following order:['ADP', 'ATP', 'AcAld', 'BPG', 'DHAP', 'F16bP', 'F6P',
                 'G1P', 'G3P', 'G6P', 'GAP', 'GLC', 'NAD', 'P2G', 'P3G', 'PEP', 'PYR', 'T6P', 'UDP', 'UTP', 'AMP', 'NADH',
                 'UDG', 'E4P', 'G6L', 'NADPH', 'P6G', 'R5P', 'Ru5P', 'S7P', 'X5P', 'NADP']
                Concentration unit: mmol/l
                cell:	Number of cells"""

        [ADP, ATP, AcAld, BPG, F16bP, G1P, G3P, GLC, NAD, P2G, P3G, PEP, PYR, T6P, UDP, UTP, AMP, NADH, UDG, G6L, NADPH,
         P6G, NADP, C, P] = y
        df_vec = np.zeros(25)
        eqspec = self.get_all(C/1000, P/1000)

        GAP = eqspec['gap']*1000
        DHAP = eqspec['dhap']*1000
        E4P = eqspec['e4p']*1000
        X5P = eqspec['x5p']*1000
        Ru5P = eqspec['ru5p']*1000
        R5P = eqspec['r5p']*1000
        F6P = eqspec['f6p']*1000
        G6P = eqspec['g6p']*1000
        S7P = eqspec['s7p']*1000
        S17P = eqspec['s17p']*1000

        df_vec[0] = self.ATPase_func(ATP, cell=cell) + self.HXK_func(ADP, ATP, G6P, GLC, T6P, cell=cell) +\
            self.PFK_func(ADP, ATP, F16bP, F6P, AMP, cell=cell) + \
            self.udp_to_utp_func(ATP, UDP, cell=cell) - self.AK_func(ADP, ATP, AMP, cell=cell) - \
            self.AK_func(ADP, ATP, AMP, cell=cell) - self.PGK_func(ADP, ATP, BPG, P3G, cell=cell) - \
            self.PYK_func(ADP, ATP, F16bP, PEP, PYR, cell=cell)

        df_vec[1] = self.AK_func(ADP, ATP, AMP, cell=cell) + self.PGK_func(ADP, ATP, BPG, P3G, cell=cell) + \
            self.PYK_func(ADP, ATP, F16bP, PEP, PYR, cell=cell) - self.ATPase_func(ATP, cell=cell) - \
            self.HXK_func(ADP, ATP, G6P, GLC, T6P, cell=cell) - self.PFK_func(ADP, ATP, F16bP, F6P, AMP, cell=cell) - \
            self.udp_to_utp_func(ATP, UDP, cell=cell)

        df_vec[2] = self.PDC_func(PYR, cell=cell) - self.ADH_func(AcAld, NAD, NADH, cell=cell) - \
            self.acetate_branch_func(AcAld, NAD, cell=cell)

        df_vec[3] = self.TDH_func(BPG, GAP, NAD, NADH, cell=cell) - self.PGK_func(ADP, ATP, BPG, P3G, cell=cell)
        df_vec[4] = self.PFK_func(ADP, ATP, F16bP, F6P, AMP, cell=cell) - self.FBA_func(DHAP, F16bP, GAP, cell=cell)
        df_vec[5] = self.PGM_func(G1P, G6P, cell=cell) - self.UGP_func(G1P, UTP, UDG, cell=cell)
        df_vec[6] = self.GPD_func(ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=cell) - self.GPP_func(G3P, cell=cell)
        df_vec[7] = self.HXT_func(GLC, cell=cell) - self.HXK_func(ADP, ATP, G6P, GLC, T6P, cell=cell)
        df_vec[8] = self.ADH_func(AcAld, NAD, NADH, cell=cell) + \
            self.GPD_func(ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=cell) - \
            self.TDH_func(BPG, GAP, NAD, NADH, cell=cell) - self.acetate_branch_func(AcAld, NAD, cell=cell)

        df_vec[9] = self.GPM_func(P2G, P3G, cell=cell) - self.ENO_func(P2G, PEP, cell=cell)
        df_vec[10] = self.PGK_func(ADP, ATP, BPG, P3G, cell=cell) - self.GPM_func(P2G, P3G, cell=cell)
        df_vec[11] = self.ENO_func(P2G, PEP, cell=cell) - self.PYK_func(ADP, ATP, F16bP, PEP, PYR, cell=cell)
        df_vec[12] = self.PYK_func(ADP, ATP, F16bP, PEP, PYR, cell=cell) - self.PDC_func(PYR, cell=cell)
        df_vec[13] = self.TPS_func(G6P, UDG, cell=cell) - self.TPP_func(T6P, cell=cell)
        df_vec[14] = self.TPS_func(G6P, UDG, cell=cell) - self.udp_to_utp_func(ATP, UDP, cell=cell)
        df_vec[15] = self.udp_to_utp_func(ATP, UDP, cell=cell) - self.UGP_func(G1P, UTP, UDG, cell=cell)
        df_vec[16] = self.AK_func(ADP, ATP, AMP, cell=cell)
        df_vec[17] = self.TDH_func(BPG, GAP, NAD, NADH, cell=cell) + self.acetate_branch_func(AcAld, NAD, cell=cell) - \
            self.ADH_func(AcAld, NAD, NADH, cell=cell) - self.GPD_func(ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=cell)

        df_vec[18] = self.UGP_func(G1P, UTP, UDG, cell=cell) - self.TPS_func(G6P, UDG, cell=cell)
        df_vec[19] = self.ZWF_func(G6P, G6L, NADPH, NADP, t, cell=cell) - self.SOL_func(G6L, P6G, cell=cell)
        df_vec[20] = self.GND_func(NADPH, P6G, Ru5P, NADP, cell=cell) + \
            self.ZWF_func(G6P, G6L, NADPH, NADP, t, cell=cell) - self.NADPH_oxidase_func(NADPH, cell=cell)

        df_vec[21] = self.SOL_func(G6L, P6G, cell=cell) - self.GND_func(NADPH, P6G, Ru5P, NADP, cell=cell)
        df_vec[22] = self.NADPH_oxidase_func(NADPH, cell=cell) - self.GND_func(NADPH, P6G, Ru5P, NADP, cell=cell) - \
            self.ZWF_func(G6P, G6L, NADPH, NADP, t, cell=cell)

        df_vec[23] = 6 * self.FBA_func(DHAP, F16bP, GAP, cell=cell) + \
            6 * self.HXK_func(ADP, ATP, G6P, GLC, T6P, cell=cell) + \
            5 * self.GND_func(NADPH, P6G, Ru5P, NADP, cell=cell) - \
            3 * self.GPD_func(ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=cell) - \
            6 * self.PFK_func(ADP, ATP, F16bP, F6P, AMP, cell=cell) - \
            6 * self.PGM_func(G1P, G6P, cell=cell) - \
            3 * self.TDH_func(BPG, GAP, NAD, NADH, cell=cell) - \
            6 * self.TPS_func(G6P, UDG, cell=cell) - \
            6 * self.ZWF_func(G6P, G6L, NADPH, NADP, t, cell=cell) - \
            4 * self.E4P_sink_func(E4P, cell=cell) - \
            5 * self.R5P_sink_func(R5P, cell=cell)

        df_vec[24] = 2 * self.FBA_func(DHAP, F16bP, GAP, cell=cell) + \
            1 * self.HXK_func(ADP, ATP, G6P, GLC, T6P, cell=cell) + \
            1 * self.GND_func(NADPH, P6G, Ru5P, NADP, cell=cell) - \
            1 * self.GPD_func(ADP, ATP, DHAP, F16bP, G3P, NAD, NADH, cell=cell) - \
            1 * self.PFK_func(ADP, ATP, F16bP, F6P, AMP, cell=cell) - \
            1 * self.PGM_func(G1P, G6P, cell=cell) - \
            1 * self.TDH_func(BPG, GAP, NAD, NADH, cell=cell) - \
            1 * self.TPS_func(G6P, UDG, cell=cell) - \
            1 * self.ZWF_func(G6P, G6L, NADPH, NADP, t, cell=cell) - \
            1 * self.E4P_sink_func(E4P, cell=cell) - \
            1 * self.R5P_sink_func(R5P, cell=cell) - \
            1 * self.SHB17(S17P, F16bP, t)
        return np.array(df_vec)

    def integrate(self, start, stop, steps, initial=[]):
        """
        Numerically integrates the ODE system of the combined glycolysis and pentose phosphate pathway model using ode.

        :param start: Starting Point (f(start) = initial)
        :param stop: End point of integration
        :param steps: Number of integration steps (w/o the initial value)
        :param initial: Initial concentrations, default are the published initial concentrations
        :return: Array of concentrations of shape (steps+1, 25), Vector of time steps
        """
        r = ode(self.glycolysis_ppp_system_deriv).set_integrator('lsoda')
        if not initial:
            r.set_initial_value(self.init_arguments, start)
            y = [self.init_arguments]
        else:
            r.set_initial_value(initial, start)
            y = [initial]

        t = [start]
        dt = (stop-start)/steps
        while r.t < stop and r.successful():
            r.integrate(r.t + dt)
            y.append(r.y)
            t.append(r.t)
        return np.array(y), np.array(t)

class Plots:

    def __init__(self, m):
        self.model = m

    def plotSHBchange(self):
        vms = np.linspace(0, 6, 40)
        res = []
        for i in vms:
            try:
                self.model.shbparams['vm'] = i
                res.append(self.model.steadystate(kmax=50000)[-2:])
            except ConstraintError:
                break
        res = np.array(res)
        self.plotCP(res, vms[:res.shape[0]], SHB=True)

    def plotCP(self, res, t, file, SHB=False):
        """
        Plots the sugar-phosphate distribution (Metabolites of the EQ-Module for any integration result and compares
        it to the dataset.
        Plots it into a pdf file
        """
        if SHB:
            Cs = res[:, 0] / 1000
            Ps = res[:, 1] / 1000
        else:
            Cs = res[:, 23]/1000
            Ps = res[:, 24]/1000
        colors = {'gap': 'black', 'dhap': 'blue', 'e4p': 'red', 'x5p': 'magenta', 'ru5p': 'brown', 'r5p': 'yellow',
                  'f6p': 'green', 'g6p': 'olive', 's7p': 'orange', 's17p': 'cyan', 'o8p': 'grey', 'o18p': 'indigo'}
        ms_data = pd.read_csv('data/ms_data_transpose_filled.csv')
        gap = []
        dhap = []
        e4p = []
        x5p = []
        ru5p = []
        r5p = []
        g6p = []
        f6p = []
        s7p = []
        s17p = []
        o8p = []
        o18p = []
        for i in range(len(Cs)):
            try:
                dist = self.model.get_all(Cs[i], Ps[i])
            except ConstraintError:
                dist = self.model.get_all(0, 0)
            gap.append(dist['gap']*1e6)
            dhap.append(dist['dhap']*1e6)
            e4p.append(dist['e4p']*1e6)
            x5p.append(dist['x5p']*1e6)
            ru5p.append(dist['ru5p']*1e6)
            r5p.append(dist['r5p']*1e6)
            g6p.append(dist['g6p']*1e6)
            f6p.append(dist['f6p']*1e6)
            s7p.append(dist['s7p']*1e6)
            s17p.append(dist['s17p']*1e6)
            o8p.append(dist['o8p']*1e6)
            o18p.append(dist['o18p']*1e6)

        with PdfPages(file) as pdf:
            fig = plt.figure(figsize=(10, 13))
            ax1 = fig.add_subplot(4, 1, 1)
            ax2 = fig.add_subplot(4, 1, 2)
            ax3 = fig.add_subplot(4, 1, 3)
            ax4 = fig.add_subplot(4, 1, 4)
            line = 3
            ax1.plot(t, e4p, '-', color=colors['e4p'], label='E4P', linewidth=line)
            ax1.plot(ms_data['Time'], ms_data['E4P'], '.--', color=colors['e4p'], label='E4P (Data)')
            ax1.plot(t, gap, '-', color=colors['gap'], label='GAP', linewidth=line)
            ax1.plot(ms_data['Time'], ms_data['G3P'], '.--', color=colors['gap'], label='GAP (Data)')
            ax1.plot(t, dhap, '-', color=colors['dhap'], label='DHAP', linewidth=line)
            ax1.plot(ms_data['Time'], ms_data['DHAP'], '.--', color=colors['dhap'], label='DHAP (Data)')

            ax2.plot(t, x5p, '-', color=colors['x5p'], label='X5P', linewidth=line)
            ax2.plot(ms_data['Time'], ms_data['Xylulose 5-phosphate*'], '.--', color=colors['x5p'], label='X5P (Data)')
            ax2.plot(t, ru5p, '-', color=colors['ru5p'], label='Ru5P', linewidth=line)
            ax2.plot(ms_data['Time'], ms_data['Ru5P'], '.--', color=colors['ru5p'], label='Ru5P (Data)')
            ax2.plot(t, r5p, '-', color=colors['r5p'], label='R5P', linewidth=line)
            ax2.plot(ms_data['Time'], ms_data['R5P'], '.--', color=colors['r5p'], label='R5P (Data)')
            ax2.plot(t, s17p, '-', color=colors['s17p'], label='S17P', linewidth=line)
            ax2.plot(ms_data['Time'], ms_data['SBP*'], '.--', color=colors['s17p'], label='S17P (Data)')

            ax3.plot(t, s7p, '-', color=colors['s7p'], label='S7P', linewidth=line)
            ax3.plot(ms_data['Time'], ms_data['S7P'], '.--', color=colors['s7p'], label='S7P (Data)')
            ax3.plot(t, f6p, '-', color=colors['f6p'], label='F6P', linewidth=line)
            ax3.plot(ms_data['Time'], ms_data['F6P'], '.--', color=colors['f6p'], label='F6P (Data)')
            ax3.plot(t, g6p, '-', color=colors['g6p'], label='G6P', linewidth=line)
            ax3.plot(ms_data['Time'], ms_data['G6P'], '.--', color=colors['g6p'], label='G6P (Data)')

            ax4.plot(t, o18p, '-', color=colors['o18p'], label='O18P', linewidth=line)
            ax4.plot(ms_data['Time'], ms_data['O8P*'], '.--', color=colors['o8p'], label='O8P (Data)')
            ax4.plot(t, o8p, '-', color=colors['o8p'], label='O8P', linewidth=line)
            ax4.plot(ms_data['Time'], ms_data['OBP*'], '.--', color=colors['o18p'], label='O18P (Data)')

            legends = (1.1, 1.0)
            fs = 11
            ax1.legend(bbox_to_anchor=legends, fontsize=fs)
            # ax1.set_xlabel(r'$Time \ [s]$')
            ax1.set_ylabel(r'$Concentration \ [\mu M]$')
            ax2.legend(bbox_to_anchor=legends, fontsize=fs)
            # ax2.set_xlabel(r'$Time \ [s]$')
            ax2.set_ylabel(r'$Concentration \ [\mu M]$')
            ax3.legend(bbox_to_anchor=legends, fontsize=fs)
            # ax3.set_xlabel(r'$Time \ [s]$')
            ax3.set_ylabel(r'$Concentration \ [\mu M]$')
            ax4.legend(bbox_to_anchor=legends, fontsize=fs)
            ax4.set_ylabel(r'$Concentration \ [\mu M]$')
            ax4.set_xlabel(r'$Time \ [s]$')
            ax1.set_xlim(t[0], t[-1])
            ax1.set_ylim(0, 30)
            ax2.set_xlim(t[0], t[-1])
            ax2.set_ylim(0, 20)
            ax3.set_xlim(t[0], t[-1])
            ax4.set_xlim(t[0], t[-1])
            pdf.savefig()
            plt.close()

    def plotCP2(self, res, t, file, CPonly=False):
        """
        Plots the sugar-phosphate distribution (Metabolites of the EQ-Module for any integration result
        Plots it into a pdf file
        """
        if CPonly:
            Cs = res[:, 0] / 1000
            Ps = res[:, 1] / 1000
        else:
            Cs = res[:, 23]/1000
            Ps = res[:, 24]/1000
        colors = {'gap': 'black', 'dhap': 'blue', 'e4p': 'red', 'x5p': 'magenta', 'ru5p': 'brown', 'r5p': 'yellow',
                  'f6p': 'green', 'g6p': 'olive', 's7p': 'orange', 's17p': 'cyan', 'o8p': 'grey', 'o18p': 'indigo'}
        gap = []
        dhap = []
        e4p = []
        x5p = []
        ru5p = []
        r5p = []
        g6p = []
        f6p = []
        s7p = []
        s17p = []
        o8p = []
        o18p = []
        for i in range(len(Cs)):
            try:
                dist = self.model.get_all(Cs[i], Ps[i])
            except ConstraintError:
                dist = self.model.get_all(0, 0)
            gap.append(dist['gap']*1e6)
            dhap.append(dist['dhap']*1e6)
            e4p.append(dist['e4p']*1e6)
            x5p.append(dist['x5p']*1e6)
            ru5p.append(dist['ru5p']*1e6)
            r5p.append(dist['r5p']*1e6)
            g6p.append(dist['g6p']*1e6)
            f6p.append(dist['f6p']*1e6)
            s7p.append(dist['s7p']*1e6)
            s17p.append(dist['s17p']*1e6)
            o8p.append(dist['o8p']*1e6)
            o18p.append(dist['o18p']*1e6)
        with PdfPages(file) as pdf:
            plt.figure(figsize=(11, 6))
            plt.plot(t, gap, '-', label='gap', color=colors['gap'], linewidth=3)
            plt.plot(t, dhap, '-', label='dhap', color=colors['dhap'], linewidth=3)
            plt.plot(t, e4p, '-', label='e4p', color=colors['e4p'], linewidth=3)
            plt.plot(t, x5p, '-', label='x5p', color=colors['x5p'], linewidth=3)
            plt.plot(t, ru5p, '-', label='ru5p', color=colors['ru5p'], linewidth=3)
            plt.plot(t, r5p, '-', label='r5p', color=colors['r5p'], linewidth=3)
            plt.plot(t, g6p, '-', label='g6p', color=colors['g6p'], linewidth=3)
            plt.plot(t, f6p, '-', label='f6p', color=colors['f6p'], linewidth=3)
            plt.plot(t, s7p, '-', label='s7p', color=colors['s7p'], linewidth=3)
            plt.plot(t, s17p, '-', label='s17p', color=colors['s17p'], linewidth=3)
            plt.plot(t, o8p, '-', label='o8p', color=colors['o8p'], linewidth=3)
            plt.plot(t, o18p, '-', label='o18p', color=colors['o18p'], linewidth=3)
            fs = 15
            plt.legend(fontsize=fs)
            plt.ylabel(r'Concentration $[\mu M]$', size=fs)
            plt.xlabel(r'Time $[s]$', size=fs)
            plt.xticks(fontsize=fs)
            plt.yticks(fontsize=fs)
            pdf.savefig()
            plt.close()

    def plotBoundaries(self, res, t):
        plt.plot(t, res[:, 2], label='AcAld')
        plt.plot(t, res[:, 13], label='T6P')
        plt.plot(t, res[:, 3], label='G3P')
        plt.legend()
        plt.show()

    def plotBoundaryrates(self, res, t):
        adh = [self.model.ADH_func(x[0], x[1], x[2]) for x in zip(res[:, 2], res[:, 8], res[:, 17])]
        act = [self.model.acetate_branch_func(x[0], x[1]) for x in zip(res[:, 2],  res[:, 8])]
        plt.plot(t, adh, label='rate ADH')
        plt.plot(t, act, label='rate Acetate Branch')
        plt.legend()
        plt.show()

    def plotCurrencyMetabolites(self, res, t, file):
        nadph = res[:, 20]
        nadp = res[:, 22]
        nad = res[:, 8]
        nadh = res[:, 17]
        atp = res[:, 1]
        adp = res[:, 0]
        amp = res[:, 16]

        with PdfPages(file) as pdf:
            fig = plt.figure(figsize=(11, 6))
            ax1 = fig.add_subplot(2, 1, 1)
            ax2 = fig.add_subplot(2, 1, 2)
            line = 3
            ax1.plot(t, nadph*1000, '-', label="NADPH", color='darkblue', linewidth=line)
            ax1.plot(t, nadp*1000, '-', label="NADP", color='blue', linewidth=line)

            ax1.plot(t, nadh*1000, '-', label='NADH', color='darkgreen', linewidth=line)
            ax1.plot(t, nad*1000, '-', label='NAD', color='green', linewidth=line)

            ax2.plot(t, adp*1000, '-', label='ADP', color='red', linewidth=line)
            ax2.plot(t, atp*1000, '-', label='ATP', color='darkred', linewidth=line)
            ax2.plot(t, amp*1000, '-', label='AMP', color='black', linewidth=line)

            ax1.legend()
            ax2.legend()
            ax2.set_xlabel('Time $[s]$')
            ax1.set_ylabel('Concentration $[\mu M]$')
            ax2.set_ylabel('Concentration $[\mu M]$')
            ax1.set_xlim(t[0], t[-1])
            ax2.set_xlim(t[0], t[-1])

            pdf.savefig()
            plt.close()

    def plotCtoP(self, res, t, file):
        """
        Plots the C to P ratio of a numeric simulation of the system.
        :param res: result vector from the function GlyPPPmodelosc.integrate
        :param t: Time vector for parameter "res
        :param file: "output file of the plot (Pdf)
        """
        model = PPPmodel()
        cp = []
        # following only useable, if you have access to the data
        # data = DataImport()
        # for i in range(40):
        #    c, p = data.data_access(i)
        #    cp.append(c / p)
        with PdfPages(file) as pdf:
            plt.figure(figsize=(11, 6))
            #plt.plot(pd.read_csv('data/ms_data_transpose_filled.csv')['Time'], cp, label='C/P (Data)')
            plt.plot(t, res[:, 23] / res[:, 24], label='C/P (Model)')
            plt.legend()
            pdf.savefig()
            plt.close()


def main():
    model = GlyPPPmodelosc()
    plots = Plots(model)
    y, t = model.integrate(0, 12000, 20000, initial=model.init_arg_data)
    plots.plotCP2(y, t, 'osc2.pdf')
    plots.plotCtoP(y, t, 'cTop.pdf')

if __name__ == '__main__':
    main()
