# Mathematical Model of the Pentose Phophate Pathway in Yeast

This repository contains the python implementation of a Bachelor's thesis with
the title:

### "Mathematical Models of the Pentose Phosphate Pathway in Yeast"

The work was supervised by Jun.-Prof. Oliver Ebenhöh and realized in the

&nbsp;
&nbsp;

Institute for Theoretical and Quantitative Biology

Heinrich-Heine-Universität Düsseldorf

Building: 25.32.03.24

Universitätsstraße 1

40225 Düsseldorf 

&nbsp;
&nbsp;

For further information, questions or to get access to the thesis contact:
Tim Daniel Rose (tim.rose@hhu.de) or
oliver Ebenhoeh (oliver.ebenhoeh@hhu.de)

&nbsp;
&nbsp;

All models are implemented in python3.5
The requirements for all used packages are avilable in the file "req.txt"
